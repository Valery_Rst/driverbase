package util;

import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;
import java.net.URL;

public class Transporter {
    private static final long DELAY = 150;

    public static void transportTo(AnchorPane container, URL to) {
        new java.util.Timer().schedule(
                new java.util.TimerTask() {
                    @Override
                    public void run() {
                        Platform.runLater(() -> {
                            try {
                                container.getChildren().setAll((Node) FXMLLoader.load(to));
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        });
                    }}, DELAY);
    }
}