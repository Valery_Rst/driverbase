package util

import data.model.Driver
import data.model.driver.*
import java.sql.PreparedStatement

object DriverParser {

    fun convertIntoObject(guid: String, surname: String, name: String, patronymic: String, passport: String,
                          registrationAddress: String, liveAddress: String, place: String, post: String, phone: String,
                          email: String): Driver {

        val driverGuid = DriverGUID(null, guid)
        val driverInfo = DriverInfo(driverGuid, name, surname, patronymic)
        val driverPassport = DriverPassport(driverGuid, passport.substring(0, 4), passport.substring(4))
        val driverAddress = DriverAddress(driverGuid, registrationAddress, liveAddress)
        val driverJob = DriverJob(driverGuid, place, post)
        val driverContacts = DriverContacts(driverGuid, phone, email)

        return Driver(driverInfo, driverPassport, driverAddress, driverJob, driverContacts)
    }


    fun convertIntoObject(data: PreparedStatement): Driver {
        return null;
    }
}