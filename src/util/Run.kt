package util

import data.Sources
import domain.util.LocateStorage
import java.net.URL

fun locationFactor(): URL? {
    return if (LocateStorage.hasSavedLocation())
        LocateStorage.getLocation() else Root.toURL(Sources.launch)
}