package util

import java.net.URL

object Root {
    fun toURL(to: String?): URL? {
        return javaClass.getResource(to)
    }
}