package presentation.view;

import domain.state.FormStateError;
import domain.viewmodel.SignUpViewModel;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import presentation.pattern.CloseableControllerView;
import util.Root;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class SignUpView extends CloseableControllerView {

    @FXML private TextField guid;
    @FXML private TextField surname;
    @FXML private TextField name;
    @FXML private TextField patronymic;
    @FXML private TextField passport;
    @FXML private TextField registrationAddress;
    @FXML private TextField liveAddress;
    @FXML private TextField place;
    @FXML private TextField post;
    @FXML private TextField phone;
    @FXML private TextField email;
    @FXML private TextField photo;
    @FXML private TextField note;
    @FXML private AnchorPane pane;
    private List<TextField> mainFields;

    @Override
    public void prep() {
        super.setContainer(pane);
    }

    @FXML
    private void transportToBack() {
        this.doTransportTo(Root.INSTANCE.toURL(launch));
    }

    @FXML
    private void doRegistration() {
        super.lockDown.freeViewLockdown();
        this.mainFields = Arrays.asList(guid, surname, name, patronymic, passport,
                registrationAddress, liveAddress, passport, phone, email);

        if (isMainFieldsEmpty()) {
            SignUpViewModel.INSTANCE.setFormState(FormStateError.EMPTY_FIELD);
            super.drawErrorAlert(SignUpViewModel.INSTANCE.getFormState());
            return;
        }

        primaryWriteDriver();
        if (SignUpViewModel.INSTANCE.validate()) {
            SignUpViewModel.INSTANCE.createDriver(SignUpViewModel.INSTANCE.getDriver());
        } else {
            super.drawErrorAlert(SignUpViewModel.INSTANCE.getFormState());
        }
    }

    private boolean isMainFieldsEmpty() {
        AtomicBoolean valid = new AtomicBoolean(false);
        this.mainFields.forEach(field -> {
            if (field.getText().trim().isEmpty())
                valid.set(true);
        });
        return valid.get();
    }

    private void primaryWriteDriver() {
        SignUpViewModel.INSTANCE.initDriver(guid.getText().trim(), surname.getText().trim(), name.getText().trim(),
                patronymic.getText().trim(), passport.getText().trim(), registrationAddress.getText().trim(),
                liveAddress.getText().trim(), place.getText().trim(), post.getText().trim(), phone.getText().trim(),
                email.getText().trim());
    }
}