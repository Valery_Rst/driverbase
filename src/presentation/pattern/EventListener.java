package presentation.pattern;

public interface EventListener {
    void prep();
    void update();
    void exit();
}