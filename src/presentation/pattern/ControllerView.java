package presentation.pattern;

import data.Sources;
import domain.util.LocateStorage;
import domain.util.LockDown;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.layout.AnchorPane;
import util.*;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


public abstract class ControllerView implements Initializable, Sources, EventListener {

    private AnchorPane container;
    private final ScheduledExecutorService scheduler;
    protected Alert alert;
    protected LockDown lockDown;

    protected ControllerView() {
        this.lockDown = new LockDown();
        this.scheduler = Executors.newSingleThreadScheduledExecutor();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        prep();
        LocateStorage.INSTANCE.saveLocation(location);
        runtimeService();
    }

    @Override
    public void exit() {
        this.scheduler.shutdown();
    }

    protected void setContainer(AnchorPane container) {
        this.container = container;
    }

    protected void doTransportTo(URL to) {
        exit();
        RebaseKt.transportTo(this.container, to);
        //Transporter.transportTo(this.container, to);
    }

    protected void drawErrorAlert(String content) {
        alert = new Alert(Alert.AlertType.ERROR, content);
        alert.showAndWait();
    }

    private void runtimeService() {
        this.scheduler.scheduleAtFixedRate(this::update, 0, 1, TimeUnit.SECONDS);
    }
}