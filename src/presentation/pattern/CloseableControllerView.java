package presentation.pattern;

import javafx.application.Platform;
import util.Root;

public abstract class CloseableControllerView extends ControllerView implements CloseableListener {

    @Override
    public void update() {
        Platform.runLater(() -> {
            super.lockDown.decViewLockdown();
            System.out.println(lockDown.getViewLockdown());
            if (rule()) {
                super.lockDown.freeViewLockdown();
                close();
            }
        });
    }

    @Override
    public boolean rule() {
        return super.lockDown.isEmptyViewLockdown();
    }

    @Override
    public void close() {
        doTransportTo(Root.INSTANCE.toURL(launch));
    }
}