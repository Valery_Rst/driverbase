package presentation;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import data.Sources;

import java.io.IOException;
import java.util.Objects;

import static util.RunKt.locationFactor;

public class Main extends Application implements Sources {

    @Override
    public void start(Stage primaryStage) throws IOException {
        AnchorPane container = FXMLLoader.load(Objects.requireNonNull(locationFactor()));
        primaryStage.setScene(new Scene(container));
        primaryStage.show();
    }

    public static void main(String[] args) {
//        if (DbConnector.getInstance().connection() != null) {
//            System.out.println(true);
//        }
        launch(args);
    }

    @Override
    public void stop() {
        System.exit(1);
    }
}