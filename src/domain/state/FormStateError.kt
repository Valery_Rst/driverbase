package domain.state

enum class FormStateError {
    EMPTY_FIELD, EMAIL_FAIL, PHOTO_FAIL;

    fun assignedMessage(): String =
            when (this) {
                EMPTY_FIELD -> "Не заполнены обязательные поля ввода"
                EMAIL_FAIL -> "Проверьте корректность вводимого E-mail"
                PHOTO_FAIL -> "Проблема с фото: " +
                        "\nРазмер файла фото не должен превышать 2 МБ, требуемый размер фото - 3х4, " +
                        "\nподдерживаются только PNG и JPG, ориентация фото должна быть вертикальной"
            }
}