package domain.validator

private const val emailPattern: String = "[a-zA-Z0-9]+@[a-zA-Z0-9]+\\.[a-zA-Z]{2,}"

fun validEmail(email: String, callback: () -> Unit): Boolean  =
        when {
            !email.matches(Regex(emailPattern)) -> {
                callback()
                false
            }
            else -> true
        }