package domain.util;

public class LockDown {

    private static final long LOCKDOWN = 60;
    private long authLockdown = LOCKDOWN;
    private long viewLockdown = LOCKDOWN;

    public long getAuthLockdown() {
        return this.authLockdown;
    }

    public long getViewLockdown() {
        return this.viewLockdown;
    }

    public void decAuthLockdown() {
        if (this.authLockdown != 0) {
            this.authLockdown -= 1;
        }
    }

    public void decViewLockdown() {
        if (this.viewLockdown != 0) {
            this.viewLockdown -= 1;
        }
    }

    public boolean isEmptyAuthLockdown() {
        return this.authLockdown == 0;
    }

    public boolean isEmptyViewLockdown() {
        return this.viewLockdown == 0;
    }

    public void freeAuthLockdown() {
        this.authLockdown = LOCKDOWN;
    }

    public void freeViewLockdown() {
        this.viewLockdown = LOCKDOWN;
    }
}