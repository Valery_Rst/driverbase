package data.repository

import data.database.DbProvider
import data.model.Driver
import java.util.*

object DriverRepository {

    private const val SQL_DRIVER_INSERT = ""
    private const val SQL_DRIVER_SELECT = ""
    private const val SQL_DRIVER_UPDATE = ""

    fun createDriver(driver: Driver?) {
        DbProvider().use { provider -> {
            provider.updatableQuery(SQL_DRIVER_INSERT)
        }}
    }

    fun updateDriver(driver: Driver?) {
        DbProvider().use { provider -> {
            provider.updatableQuery(SQL_DRIVER_UPDATE)
        }}
    }

    fun getDrivers(): List<Driver?>? {
        DbProvider().use { provider -> {
            provider.selectableQuery(SQL_DRIVER_SELECT)
        }}
        return Collections.EMPTY_LIST as List<Driver?>?
    }
}