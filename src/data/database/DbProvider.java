package data.database;

import java.io.Closeable;
import java.sql.*;

public class DbProvider implements Closeable {

    private final Connection connection;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;
    private Statement statement;

    public DbProvider() {
        this.connection = DbConnector.INSTANCE.connection();
        initStatement();
    }

    public PreparedStatement getPreparedStatement() {
        return this.preparedStatement;
    }

    public ResultSet getResultSet() {
        return this.resultSet;
    }

    @Override
    public void close() {
        closeConnection();
        closeStatement();
        closePreparedStatement();
        closeResultSet();
    }

    public void updatableQuery(String sqlQuery) {
        try {
            this.preparedStatement = this.connection.prepareStatement(sqlQuery);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("SQLException in insertQuery(String sqlQuery)");
        }
    }

    public void selectableQuery(String sqlQuery) {
        try {
            this.resultSet = this.statement.executeQuery(sqlQuery);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("SQLException in selectQuery(String sqlQuery)");
        }
    }

    private void initStatement() {
        try {
            this.statement = this.connection.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("SQLException in initStatement()");
        }
    }

    private void closeConnection() {
        try {
            this.connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("SQLException in closeConnection()");
        }
    }

    private void closeResultSet() {
        try {
            this.resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("SQLException in closeResultSet()");
        }
    }

    private void closeStatement() {
        try {
            this.statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("SQLException in closeStatement()");
        }
    }

    private void closePreparedStatement() {
        try {
            this.preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("SQLException in closePreparedStatement()");
        }
    }
}