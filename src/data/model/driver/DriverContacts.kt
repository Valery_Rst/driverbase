package data.model.driver

data class DriverContacts (
        val id: DriverGUID ?= null,
        val phone: String ?= null,
        val email: String ?= null
)