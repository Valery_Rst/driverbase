package data.model.driver

data class DriverInfo (
        val id: DriverGUID ?= null,
        val name: String ?= null,
        val surname: String ?= null,
        val patronymic: String ?= null
)