package data.model.driver

data class DriverJob (
        val id: DriverGUID ?= null,
        val place: String ?= null,
        val post: String ?= null
)