package data.model

data class Inspector (
        val login: String,
        val password: String
)